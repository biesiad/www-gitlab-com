---
layout: markdown_page
title: "Category Direction - Continuous Integration"
description: "Continuous Integration is an important part of any software development pipeline. It must be easy to use, reliable, and accurate. Learn more here!"
canonical_path: "/direction/verify/continuous_integration/"
---

- TOC
{:toc}

## Continuous Integration

Continuous Integration (CI) is an important part of any software development cycle, and defined as part of the [Verify stage](/direction/ops/#verify) here at GitLab. As declared in our Ops Section [direction](/direction/ops/#pipelines-as-code), we recognize a key advantage of GitLab CI is that we can define pipelines as code, while making CI easy to use, reliable, and accurate in terms of its results. We are very proud that we are recognized as [the leading CI/CD tool on the market](/blog/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/), as well as a leader in the 2019 Q3 [Cloud Native CI Wave](/analysts/forrester-cloudci19/), and it's important for us that we continue to innovate in this area and provide not just a "good enough" solution, but a [speedy and reliable one](/direction/ops/#speedy-reliable-pipelines).

Making it easy to run a pipeline is our first focus and this applies to both running a pipeline manually as well as triggering one automatically when submitting a code commit or a merge request. In addition, we want to provide data for examining your pipeline's performance, so that you can optimize CI configurations to make your pipelines run more efficiently.

## Additional Resources

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Integration)
- [Overall Vision of the Verify stage](/direction/ops/#verify)
- [JTBD Overview](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-execution/jtbd/)

For specific information and features related to authoring and pipelines, check out [Pipeline Authoring](/direction/verify/pipeline_authoring/). For work related to Pipeline Abuse Prevention, see the [Category page](https://about.gitlab.com/direction/anti-abuse/instance_resiliency/). 

You may also be looking for one of the following related product direction pages: [GitLab Runner](/direction/verify/runner_core/), [Continuous Delivery](/direction/release/continuous_delivery/), [Release stage](/direction/ops/#release), or [Jenkins Importer](/direction/verify/jenkins_importer/). 

## Who we are focusing on? 

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Continuous Integration, our "What's Next & Why" are targeting the following personas, as ranked by priority for support: 

1. [Sasha - Software Developer](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer)
1. [Priyanka - Platform Engineer](https://about.gitlab.com/handbook/product/personas/#priyanka-platform-engineer)
1. [Delaney - Development Team Lead](https://about.gitlab.com/handbook/product/personas/#delaney-development-team-lead)

## What's Next & Why

As part of the [FY24 Product Themes](https://about.gitlab.com/direction/#fy24-product-investment-themes) we plan to support the following areas.

### World-class DevSecOps Experience

#### Pipeline Performance and Gitlab.com Scaling

As we think about [Speedy, Reliable Pipelines](https://about.gitlab.com/direction/ops/#speedy-reliable-pipelines) an area we can control is the time it takes to create a pipeline. In FY24 we will focus on making this part of pipeline runtime faster to contribute to the next GitLab CI job performance [benchmark](https://gitlab.com/gitlab-org/gitlab/-/issues/299196/). 

We also think about how scale can impact performance of pipelines Now that we have delivered the [Next CI/CD scale target: 20M builds per day by 2024](https://docs.gitlab.com/ee/architecture/blueprints/ci_scale/) blueprint and related scope on queueing mechanisms via [gitlab&5909](https://gitlab.com/groups/gitlab-org/-/epics/5909), primary key capacity via [gitlab#325618](https://gitlab.com/gitlab-org/gitlab/-/issues/325618), and handling large amounts of data via [gitlab&6009](https://gitlab.com/groups/gitlab-org/-/epics/6009), and have delivered our [CI/CD Time Decay](https://docs.gitlab.com/ee/architecture/blueprints/ci_data_decay/) blueprint, we are ready to begin Phase II of CI Scaling: partitioning data with the CI/CD Time Decay pattern.

The first track of effort will be focused on partitioning the active CI/CD tables via [gitlab&5417](https://gitlab.com/groups/gitlab-org/-/epics/5417) and the second track is to partition queuing tables via [gitlab&7438](https://gitlab.com/groups/gitlab-org/-/epics/7438).

[Roadmap View](https://gitlab.com/groups/gitlab-org/-/roadmap?state=opened&sort=end_date_asc&layout=WEEKS&timeframe_range_type=CURRENT_QUARTER&label_name%5B%5D=Category%3AContinuous+Integration+Scaling&progress=WEIGHT&show_progress=true&show_milestones=true&milestones_type=ALL)

This group uses an [issue board](https://gitlab.com/groups/gitlab-org/-/boards/1372896?label_name[]=group%3A%3Apipeline%20execution&label_name[]=Category%3AContinuous%20Integration%20Scaling) and `CI Scaling` to identify the scope that is in workflow. The team is tracking progress of the partitioning work in the [documentation](https://docs.gitlab.com/ee/architecture/blueprints/ci_data_decay/pipeline_partitioning.html#iterations).

The following epics are used to coordinate the phases of CI Partitioning: 

-  [Phase 1 work - Partition 6 largest tables](https://gitlab.com/groups/gitlab-org/-/epics/5417)
-  Phase 2 work
-  Phase 3 work 
-  Phase 4 work
-  Phase 5 work
-  [Add Tools to support CI Partitioning](https://gitlab.com/groups/gitlab-org/-/epics/8476) - for Database team related work.

### Advanced Security and Compliance

#### Detection and remediation of secrets in build logs

GitLab lets users make use of various tokens that allow ability to deploy and access resources within projects (Deploy tokens and the CI_JOB_TOKEN for example). Even staying diligent these tokens could be leaked by users providing powerful capabilities to nefarious actors. To better protect users will will strive to detect and remediate GitLab provided tokens found in job logs. We are tracking this effort in the epic [gitlab&8007](https://gitlab.com/groups/gitlab-org/-/epics/8007).

#### Secure CI_JOB_TOKEN

The [CI_JOB_TOKEN](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html) makes it intuitive to access some parts of the GitLab API from within jobs to enable automation. To enhance the security of this short-lived token we will let project maintainers set which projects can use the token to interact with their project with the [next phase](https://gitlab.com/groups/gitlab-org/-/epics/6546#phase-11-ci_job_token-can-be-used-for-use-cases-like-dependabot-without-worrying-about-tokens-leaking-get-data-about-which-projects) of the CI_JOB_TOKEN workflows epic. Our vision is for this workflow to be on by default for all projects with the next major release 16.0.

Our long term vision for this feature is to allow more access through the CI_JOB_TOKEN and a granular level of control by project. Our next step towards this will be letting project owners view and edit permissions for the existing endpoints in [gitlab&9844](https://gitlab.com/groups/gitlab-org/-/epics/9844). After this we can start to add endpoints the CI_JOB_TOKEN can authenticate against like the ones listed in the epic [Secure `CI_JOB_TOKEN` permissions](https://gitlab.com/groups/gitlab-org/-/epics/6310). 

## Maturity Plan

Our current maturity is at "Complete" and the next maturity target is "Lovable"  (see our [definitions of maturity levels](/direction/maturity/)). We previously reached "Lovable" in 2017, after being listed a [CI Leader](https://about.gitlab.com/blog/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/). In order to maintain this lead while staying ahead of the changing DevOps landscape needs for stability, performance and quality we need to reestablish a strong foundation of the core elements for CI. As such, we are prioritizing bugs and user experience improvements, while continuing to design and validate features for future implementation that move our vision forward. The following investments will be key to moving our maturity forward in the next two quarters: 

- [Bugs related quota of compute minutes](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%20minutes&label_name[]=type::bug)
- [Simplify merge strategies with automerge](https://gitlab.com/groups/gitlab-org/-/epics/6687)

These investments will lay the ground work to deliver on the top vision items in 2023 (see [Epic#4794](https://gitlab.com/groups/gitlab-org/-/epics/4794)) which involve features under these key areas:

- [Secure CI_JOB_TOKEN Workflows](https://gitlab.com/groups/gitlab-org/-/epics/6546)
- [Make GitLab CI/CD work better with external repositories](https://gitlab.com/groups/gitlab-org/-/epics/943)

## What is Not Planned Right Now

Our strategy above to regain a category maturity of "Lovable" is two-fold - first, a renewed focus on strengthening the core features of CI that support running a pipeline; and second, deliver features that provides more users with the ability to run pipelines in a project. To execute with purpose on these plans means there are opportunities in the CI category that we will not be pursuing. While this is not an exhaustive list, below are popular features/topics representing groups of issues that are currently **not** prioritized on our roadmap.

- [Improve the experience around debugging jobs and analyzing pipelines](https://gitlab.com/groups/gitlab-org/-/epics/5022)
- CI related [notifications](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=notifications&label_name[]=group%3A%3Apipeline%20execution) (by email or via integration with other tools)
- CI related [API endpoints](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=api&label_name%5B%5D=group%3A%3Apipeline+execution&state=opened) (unless related to features on the roadmap)
- CI related [permissions](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%20permissions&label_name[]=group%3A%3Apipeline%20execution&not[label_name][]=bug) (non-bug issues)
- [Configure CI/CD Quotas by project](https://gitlab.com/gitlab-org/gitlab/-/issues/357760) going forward we will work with the community to review and merge contributions but are not planning active development on new features.

## Competitive Landscape

The majority of CI market conversation is between us, Jenkins, and GitHub Actions at this point. An example of this placement is from [Jet Brain's 5th annual Developer Ecosystem Survey](https://www.jetbrains.com/lp/devecosystem-2021/) which has placed GitLab as #2 CI solution for enterprises. Atlassian has built BitBucket Pipelines, a more modernized version of Bamboo, which is still in the early stages. Microsoft is maintaining (at least for now) Azure DevOps at the same time as GitHub Actions but for personal usage GitHub Actions is gaining traction among developers. CodeFresh and CircleCI have both released [container-based plugin model](https://steps.codefresh.io/), similar to GitHub Actions. CircleCI in particular is known for very fast startup times and we're looking to ensure we [keep up or get even faster](https://gitlab.com/groups/gitlab-org/-/epics/7290). Jenkins is largely seen as a legacy tool, and most people we speak with are interested in moving off to something more modern. We are addressing this with our [Jenkins Importer](/direction/verify/jenkins_importer) category which is designed to make this as easy as possible.

From [GitHub's 2023 Roadmap](https://github.com/orgs/github/projects/4247), we are seeing GitLab-reminiscent features which include [Pull Request Merge Queue](https://github.com/github/roadmap/issues/370), akin to [Merge Trains](https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html) with a fit-finish that we aim to make easier in [gitlab#294169](https://gitlab.com/gitlab-org/gitlab/-/issues/294169). Also to note is an emphasis on governance and controls with [Audit Log streaming](https://github.com/github/roadmap/issues/344), bringing parity to the capabilities GitLab has created with the [Compliance group's Audit Event streaming](https://docs.gitlab.com/ee/administration/audit_event_streaming.html). 

## Analyst Landscape

There are a few key findings from the Forrester Research analysts on our CI solution. GitLab is seen as capable as the solutions provided by the hyperclouds themselves, and well ahead of other neutral solutions. This can give our users flexibility when it comes to which cloud provider(s) they want to use. We are also seen as the best end to end leader, with other products  not keeping up and not providing as comprehensive solutions. What this tells us is that it is important for us to continue to innovate and make it hard or even impossible for competitors to maintain pace.

As such, our path to improving our analyst performance matches our solutions above in terms of staying ahead of our competitors.

## Top Customer Success/Sales Issue(s)

The Field teams are typically most interested in uptier features like [Premium](https://gitlab.com/groups/gitlab-org/-/issues?sort=milestone&state=opened&label_name[]=group::pipeline+execution&label_name[]=Category:Continuous+Integration&label_name[]=GitLab+Premium) and [Ultimate](https://gitlab.com/groups/gitlab-org/-/issues?sort=popularity&state=opened&label_name[]=group::pipeline+execution&label_name[]=Category:Continuous+Integration&label_name[]=GitLab+Ultimate&not[label_name][]=GitLab+Premium&not[label_name][]=GitLab+Free). The top requested issues in these tiers include a [Group Level Dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/11960) and [adding metrics for pipeline and job data to be exported for OpenTelemetry](https://gitlab.com/gitlab-org/gitlab/-/issues/301096). Also important for the sales team is [gitlab#205494](https://gitlab.com/gitlab-org/gitlab/issues/205494) which will allow for easier use of GitLab's security features when not using GitLab's CI.

## Top Customer Issue(s)

Our top customer issues ([search results](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Apipeline%20execution&label_name[]=customer)) include the following:

- [Ensure after_script is called for cancelled and timed out pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/15603)
- [Add configurable pipeline priority](https://gitlab.com/gitlab-org/gitlab/-/issues/14559)
- [Optional manual action becomes blocking when it is started](https://gitlab.com/gitlab-org/gitlab/-/issues/20237)
- [Gitlab CI - Run job on all runners that have the specific tag](https://gitlab.com/gitlab-org/gitlab/-/issues/16474)

Another item with a lot of attention is to normalize job tokens in a more flexible way, so that they can have powerful abilities when needed and still not introduce security risks ([gitlab#3559](https://gitlab.com/groups/gitlab-org/-/epics/3559)).

We also have a few issues about making variables available before includes are processed, however there is a "chicken and egg" problem here that has been difficult to solve. Child/parent pipelines solves some use cases, but not all. There are two related epics here, [Use a variable inside other variables in .gitlab-ci.yml](https://gitlab.com/groups/gitlab-org/-/epics/3589) and [Raw (unexpanded) variables MVC](https://gitlab.com/groups/gitlab-org/-/epics/1994)

## Top Internal Customer Issue(s)

Our top internal customer issues ([search results](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Apipeline%20execution&label_name[]=internal%20customer)) include the following:

- [Fail the pipeline immediately if one of the jobs fail](https://gitlab.com/gitlab-org/gitlab/-/issues/23605)
- [Add custom labels to CICD pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/26618)

In discussing the second issue with customers we have learned that there are two use cases for this: 1. Labeling before pipeline execution of things like long running integration tests which was done in [gitlab#372538](https://gitlab.com/gitlab-org/gitlab/-/issues/372538) with the new `workflow:name` keyword and 2. [Dynamic labeling](https://gitlab.com/gitlab-org/gitlab/-/issues/27133#note_1079444238) based on results within the pipeline. We will consider both as we work towards the MVC and think about the pipeline search experience.

Our top dogfooding issues ([search results](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Apipeline%20execution&label_name[]=Dogfooding)) are:

- [Group level pipeline dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/11960)
- [Public pipeline page MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/10861)

## Top Vision Item(s)

Looking to the future, we have plans to help you better monitor and understand your pipeline [epic#4794](https://gitlab.com/groups/gitlab-org/-/epics/4794). Having details about pipeline activities (such as job duration) will allow you to see and react to what's happening while your pipeline is running. Beyond using data simply for reactive purposes, we have plans for a customizable UI for historical pipeline analytics so you can see the trends that will guide your planning and decision-making. 

We'd love to create a holistic approach to automatically merging when pipelines succeed via [gitlab#8128](https://gitlab.com/gitlab-org/gitlab/-/issues/8128), which offers great collaboration between [Code Review](/direction/create/code_review/) and Continuous Integration. 

Even further into the future, we are looking to expand insights and predications of CI use to help you reduce waste via [gitlab&4915](https://gitlab.com/groups/gitlab-org/-/epics/4915).
